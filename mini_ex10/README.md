![screenshot](SS.png)


https://gitlab.com/majastoerum/ap2019/tree/master/mini_ex4/p5/empty-example for mini excersice 4

For the individual exercise I chose to revisit my mini exercise number 4.  This wasn’t the very most technically complex program but given that I wanted to create a flowchart based on the view of the user, this was in that aspect more complex than the other mini exercises, since this program calls for bigger interaction of the user. I wanted to create a flowchart that visualized the different steps the user has to go through in the program and it almost works as a guideline for the program. 
The program I chose to visualize works in the field of data capturing, and the idea of the program, was to create a simulation of all the data captured of different individuals over time. The idea was to create a program in which the user scans his/hers face, after which it is possible to read about their doings on a specific date.  

I created the flowchart to give a better understanding of the program, but it was also a little difficult to make the flow chart exactly the way the program works. For example, when the program is opened, a pop-up window will appear, in which the user has to allow the program to use the webcam. If the user doesn’t allow the program to use it, nothing really happens. It will still be possible to pick different dates and read about your doings that day – even though the camera is off. This was very difficult to implement in the flowchart, and therefore I just left it out, so that the flowchart doesn’t reflect all the possibilities of the program, but only how it SHOULD work.

>  *I had some trubble with inserting the pictures of the two flowcharts, but you can see them on Adams gitlab here: https://gitlab.com/adamnaldal/ap2019/tree/master/Mini_Exercises/Mini_ex10*

**Mono no Aware** 


The first flowchart is made upon our first idea, which we chose to call “Mono no Aware”. The idea of the program is to make a game, in which your task is to grow a cherry blossom tree. The player needs to take care of the tree, as if it was an actual plant. Therefore, the tree needs in-real-life sunlight, space and water to grow. The space depends on the pixels on the screen (the size of the browser window) and you give the water by clicking on a button regularly (as you would with a real life plant). This should give the player a very slow experience of the game as the plant will act just like a real life plant. At the same time the virtual plant will request the players screen time and space (for example when you need to turn the computerscreen against the window, when the plant needs more sunlight or when you need to enlarge and prioritise the browser window over the other windows. 

We want to make a very slow experience but that is difficult to present in a reasonable time for an exam.
Generative art can be technically difficult or “mathy” to get right. And all of the plants expression has to come through the image that is generated.
The light detection library might be tricky, it plays as an essential part in the functionality of the program, but given that we have never worked with this type of library before, we could end up with having to use a suboptimal alternative.  


**Remié Serché**


This program is a resource management game in which the player plays in the year 1242, in a French small town, as the miller called Remié Serché, whos task is to control and run the mill in the town. As the local farmers come with their corn to the mill, the player must negotiate prices.

- Difficulty with creating enough obvious conceptual linkage to the themes of the course.
Bug fixing might take a lot of time with such a complicated program.
Requires a lot of graphical elements and possibly animation, we do not want to rely too heavily on text.
Balancing the gameplay. Should give the player a sense of progression but also increased difficulty.


**Group flowchart vs individual flowchart**


The two flowcharts we created together in the group differs a bit from the flowchart I made on my own program. One of the aspects is the complexity. The two program we created together is a lot more complex than my own, and therefor the flowcharts appear more complex as well. Another aspect is the fact that the individual flowchart was made upon an already made program and code. Therefore, the flowcharts work in different ways. My own works as an instruction to the user of the program, but the flowcharts on the two other programs (that haven’t been programmed yet) also works as an instruction to the programmer. 

**Wider cultural context**

There is a clear-cut similarity between flowcharts and algorithms. As described in the text “The Multiplicity of Algorithms” by Taina Bucher, algorithms can be understood in many different ways in various contexts. But just as a flowchart, the algorithms can be seen as a step-by-step instruction or a recipe both for the programmer and for the user. The flowchart can as well be used in many different ways and with different purposes. Therefore these algorithms should not be seen as a neutral thing. 