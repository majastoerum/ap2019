![screenshot](SS1.png)
![screenshot](SS2.png)
![screenshot](SS3.png)
![screenshot](SS4.png)

https://glcdn.githack.com/majastoerum/ap2019/raw/master/mini_ex4/p5/empty-example/index.html

For this mini exercise I have experimented with the .dom-library, and the different
functions, especially different buttons and the Capture-function. 
I have also worked with a lot more variables
and different functions than I have been doing in the previous mini exercises.


The idea behind the program, was to broach a critical view on the 
enormous data capturing and selection, that constantly happens through
different sources, in different contexts. Both through
technological devices, though surveillance in different forms and so on.
Thus, I came up with the idea of making a program, which through a facial recognition system 
is able to intercept data from your whole life. Both from your activities on 
technological devices (social media, different websites, online shopping etc.), 
but also through GPS, outdoor surveillance, bank account and so on.
Thereby it is possible to go back in your past
and see almost exactly what you did on that specific date. Thus, the program is 
an example of a dystopian future, where every movement and doings are captured.


I think this topic is extremely important in these days, where the constant
flow of data is constantly increasing the need of protection and prudence. 
Especially the planned extreme surveillance of the Chinese population in 2020, 
where every citizen will be a part of a big point-system, is a thing 
that really inspired me to create this program.
Because of the technological growth and development, new opportunities have increased 
and therefore, people constantly need to relate to doings in relation to data.

