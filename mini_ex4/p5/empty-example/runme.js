var words;
var button;
var capture;
var sel;
var r;

function preload(){
words=[
  '11:00 went to work for 5 hours \n 17:00 bought three apples, one milk and one soda for 33 kr \n 19:30 took the train from Aarhus H to Esjerg',
  '7:00 woke up, went for a 3,8 km run \n listened to music on Spotify-app for 2 hours 21 minutes \n 12:30 got a parking ticket of 750 kr',
  '9:00 went to school \n 10:00 used Facebook.com and YouTube.com for 3 hours \n 14:00 got 4 likes on instagram',
  '11:30 woke up \n 13:00 bought two Chicken Salsa Cheese and five Cheese Burgers \n 16:00 walked 1,7 km \n 18:00 posted an update on Facebook.com \n 20:00 bought 5 beers and two shots',
  '2:00 got in a new relationship with Prad Bitt \n 12:00 drove the car 6 km \n 14:00 went to the doctor \n 18:00 got 3 new frindrequests on Faceboook.com',
  '6:30 took the bus for 8 km \n 8:00 took a 20 minutes long bath - average tempurature: 25°C \n 11:00 went to lunch with Betinna and Michael - you had a salmon salad'
]
}

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(53, 166, 230);

//text in the beginning
  textAlign(CENTER);
  push();
  fill(255);
  textSize(40);
  text('Scan your face and figure out, what you did in your past!', windowWidth/2, 100);
  pop();

  push();
  fill(255);
  textSize(30);
  text('Please allow us to use your webcam', windowWidth/2, 200);
  pop();

//webcam
  push();
  capture=createCapture(VIDEO);
  capture.size(320,200);
  capture.position(windowWidth/2-150,windowHeight/2+80);
  pop();

//button to start
  button=createButton('Press to scan face');
  button.style('width','300px');
  button.position(windowWidth/2-150, windowHeight/2);
  button.mousePressed(analyzing);
}

function analyzing(){
//slidedown with dates
  button.remove();
  textAlign(CENTER);
    sel = createSelect();
    sel.position(windowWidth/2-80, 300);
    sel.size(200);
    sel.option('15/03 2016');
    sel.option('23/07 2014');
    sel.option('11/12 2018');
    sel.option('02/04 2015');
    sel.changed(mySelectEvent);
}

//history from specific day
function mySelectEvent() {
  var item = sel.value();
  background(53, 166, 230);
 r=floor(random(0,words.length));
 textSize(16);
 fill(255);
 text(words[r], 850, 200);
}
