let video;
let vWidth=32;
let vHeight = 24;
let hue = 200;
let brightness;
let pixValues = [];

function setup(){
frameRate(15);
  createCanvas(windowWidth,windowHeight);
  colorMode(HSB,360,100,100,100);
  pixelDensity(1);

video=createCapture(VIDEO);
video.size(vWidth, vHeight);
video.hide();
}


function getBrightness(){
pixValues=[];
brightness=0;

video.loadPixels()
for (let y = 0; y < vHeight; y++){
  for (let x = 0; x < vWidth; x++){
    let index=(x + y * vWidth) * 4;
    let r = video._pInst.pixels[index + 0];
    let g = video._pInst.pixels[index + 1];
    let b = video._pInst.pixels[index + 2];

    pixValues.push((r + g + b)/3);
  }
}
  for(let i = 0; i < pixValues.length; i++){
    brightness += pixValues[i]
}
  brightness = floor(brightness/pixValues.length);
}

function draw(){
  background(hue,80,brightness);
  if (hue > 100) hue = 200;
  getBrightness();
}
