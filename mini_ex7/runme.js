var balls=[];
//NOB: Decides number of balls.
var NOB = 40;
function setup() {
  createCanvas(windowWidth, windowHeight);
  for(var i=0; i<NOB; i++)
  {
    balls.push(new Ball());
  }
}

function draw() {
  balls.push(new Ball());

  background(0)
  for(var i=0; i<NOB; i++)
  {
    balls[i].move();
    balls[i].disp();

    let arr = []
    let minDist = 10000;
    for(var j=0; j<NOB; j++){
      if (i != j){
      //Variable for checking distance between balls
      var d = dist(balls[i].xPos,balls[i].yPos,balls[j].xPos,balls[j].yPos);
      //variable for checking minimum distance between balls
      minDist = minDist > d ? d : minDist;
      arr[j] = d;
    }
    }
    for(var j=0; j<NOB; j++){
      if(i!=j){
        balls[i].connect(balls[j], arr[j], minDist);
      }
    }
  }
}

function Ball()
{
  //draws NOB in "random" places wihin the windows constraints.
  this.hide = false;
  this.d=1;
  this.xPos= random(this.d,width-this.d/2);
  this.yPos= random(this.d,height-this.d/2);
  this.xPosf=random(0.1,3);
  this.yPosf=random(0.1,3);

  this.disp=function(){
  }
  this.move=function()
  {
    //moves balls, including the constraints of the window-Width and -Height.
    this.xPos+=this.xPosf;
    this.yPos+=this.yPosf;
    if(this.xPos>width-this.d/2 || this.xPos<this.d/2){this.xPosf=this.xPosf*-1;}
    if(this.yPos>height-this.d/2 || this.yPos<this.d/2){this.yPosf=this.yPosf*-1;}

  }
  //Connects each ball with a line, but if a ball comes under the minimum distance of 30 the balls will be hidden and only reappear when the distance between them is over 100.
  this.connect = function(other, distance, minDist)
  {
    //hides and shows lines depending on distance between balls.
    if (minDist<30){
      this.hide = true;
    } else if (minDist > 100){
      this.hide = false;
    }
    if (this.hide){
      return;
    }
    // Connects balls with lines if the balls are not hidden. + The closer the balls are to eachother, the higher the strokeWeight.
    if(distance>1 && !this.hide && !other.hide)
    {
    strokeWeight((100/distance) > 1 ? 1 : 100/distance)
    //Color of lines
    stroke(0,250,60,90);

    line(this.xPos,this.yPos,other.xPos,other.yPos);
  }


  }
}
