https://glcdn.githack.com/majastoerum/ap2019/raw/master/mini_ex7/index.html 

![screenshot](SS1.png)
For this weeks mini exercise, I worked together with Jakob Østergaard and Thomas Farver. We agreed that we wanted to create something in relation to and inspired by the artist Sol Lewitt and his work “Wall Drawing #118”. With that as a fundament, and with the basic functions, we extended/increased the work with our own ideas and rules.

The role of the rules was to create a never-ending piece of work, which is unpredictable and constantly changing. It is in the hand of the viewer to figure out, what patterns and rules the work follows, which contributes to a more entertaining and compelling experience of the work. The role of the rules is therefore also to bring dynamic and a feeling of randomness and chaos to the work.

We used this idea of this controlled randomness as a part of the creations process of the work. We wanted to include three rules in the work:

1.	A random amount of “points”, which are randomly floating around in the canvas
2.	Whenever the distance between two points are under 30, the connection and point will disappear, but will reappear again, when it’s over 100.
3.	The thickness of the lines increases according to the proximity of the points

Thereby it was possible to create to so-called “pseudo-randomness” it the program. Because even though the patterns and motions of the program/work seem completely random, it’s still in control and based on computation and a basic set of rules. Thereby, we as programmers, to some extent, still have the control of the outcome. 
This idea of the authorship is therefore to be discussed,  wheater it is us as programmers, who have the full authorship of the result or if its only partly, as
the computer also have an impact on the result. And since it is the computer that is carrying out the work, should the computer be perveived as the owner of the full authorship
and the human as only a part of the creation? 

