![screenshot](SS.gif)

[link](https://glcdn.githack.com/majastoerum/ap2019/raw/master/mini_ex5/index.html)

For this mini exercise I chose to revise the very first mini exercise we did, since I have learned alot 
new syntaxes, functions and way of coding since the first time experiencing the world of coding.
My goal back then, was it the first place to experiement with different shapes and how to make different
objects and shapes to interact independently and in relation to eachother. 
The idea for the first mini exercise, was to create a solar system, but since I didn't have a lot
of experience back then, I ended up getting something that just looked like a couple of different
spheres, moving randomly around in the canvas. So for this mini exercise, I thought it would be fun, to 
recreate the idea of a solarsystem. 

It was now easier for me to create the different planets independently of eachother, as 
I know had experienced the push() and pop() functions. This made it possible for me to give the planets
different rotation speed, visual details and so on.
I also got the idea of creating a background of shooting stars, to give some depth to the 
illustration. This, i did by using the for-loop function, in which I determined the value for the different viriables, 
to create the illusion of the galaxy flying through the Universe.
Some of the feedback to the first mini exercise, was that the movements of the different speheres
seemed pretty random (whish they were) and that the illusion of a solar system would be clearer, if
the rotation was made more rigid, which I also tryed by creating the canvas in a 2D-form istead.


Programming is not only a tool to create or illustrate something without further meaning, but should also
be seen and experienced and understood as a cultural, political, social practice and phenomenon. According to
Bergstrom and Blackwell, programming is a tool for better understanding and communication between
human and machine. There is a lot of different practices acording to programming, which permits a lot of different ways of using programming.
Thus, programming should not be understood as a static thing, but as a tool that can be used in many different
aspects and different context.

According to the text "Appendix: Why Program?" the realtion with the programming and the digital cculture
is, that the programming allows the users to think in new and different ways. It thereby gives a better understanding
of the culture and the media systems - especially through the language can help ud cognitively. 
I agree with the many advantages in programming, and how it can provoke a lot of net possibilities, but 
I also think it is important to look at the critical aspects of the programming-world. One of the subtitles in the
text is "Socially: compuation can help to build a better world" - which I do not strongly diagree with, but
this utopian way of thinking is not neccesary how it will work in practise. It is still important to rely on human ressources 
and mind.


