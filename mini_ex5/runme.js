function setup (){
createCanvas(windowWidth, windowHeight);
}

function draw (){
background(10);

//shooting stars
push();
  for (var i = 0; i < 50; i++) {
  var x = random(windowWidth);
  var y = random(windowHeight);
  var w = random(60-120);
  var h = random(1-4);
  noStroke();
  fill(255);
  ellipse(x, y, w, h);
pop();

//ring moon
push();
stroke(255);
strokeWeight(0.5);
noFill();
ellipse(windowWidth/2, windowHeight/2, 200);
pop();

//ring earth
push();
stroke(255);
strokeWeight(0.5);
noFill();
ellipse(windowWidth/2, windowHeight/2, 340);
pop();

//ring saturn
push();
stroke(255);
strokeWeight(0.5);
noFill();
ellipse(windowWidth/2, windowHeight/2, 440);
pop();

//ring mercury
push();
stroke(255);
strokeWeight(0.5);
noFill();
ellipse(windowWidth/2, windowHeight/2, 600);
pop();


//the sun
push();
  fill(240, 230, 0);
  stroke(240,230,0,2);
  strokeWeight(30);
  ellipse(windowWidth/2, windowHeight/2, 130, 130);
pop();

spinningMoon(300);
}
}

function spinningMoon(num){

//the moon
push();
translate(width/2, height/2);
let cir =360/num*(frameCount%num)
rotate(radians(cir));
noStroke();
fill(220);
ellipse(100,0,20, 20);
pop();
spinningEarth(200);
}

function spinningEarth(num){

//the earth
push();
  translate(width/2, height/2);
  let cir =360/num*(frameCount%num)
  rotate(radians(cir));
  stroke(0,230,120);
  strokeWeight(4);
  fill(0, 220, 250);
  ellipse(170,0,30,30);
  pop();
spinningSaturn(240);
}

function spinningSaturn(num){
  //Saturn
push();
  translate(width/2, height/2);
  let cir =360/num*(frameCount%num)
  rotate(radians(cir));
  fill(244,206,109);
  ellipse(220,0,40,40);
  stroke(244,206,109);
  strokeWeight(3);
  noFill();
  ellipse(220,0,60,15);
pop();

spinningUranus (340);
}

function spinningUranus(num) {

  //Uranus
push();
  translate(width/2, height/2);
  let cir =360/num*(frameCount%num)
  rotate(radians(cir));
  fill(0,180, 230);
  ellipse(300,0,85,85);
pop();
}
