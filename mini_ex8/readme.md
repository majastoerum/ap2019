[link](https://cdn.staticaly.com/gl/DenFriskeCeasar/ap2019jakob/raw/master/mini%20ex/mini%20ex%208/index.html)

For this weeks miniexercise I worked together in group with Jakob Østergaard. Our idea was to create a hate or insult letter, inspirited by the “Love Letter” algorithm created by Christopher Strachey, in which the computer can randomly generate a short piece of text.

The program consists of an old desktop computer with a written hate letter on the screen. By pressing the “start over” button on the bottom of the screen, the “computer” will generate a new hate letter. 

We worked with these letters in two aspects, both seen from the result of the code, but also the written code itself. 
The story behind the program is initially up to the viewer to interpret, and it is essential to dig into the code to get the full understanding of the work. When you first take a look at the program it just seems like plane insult letter, but when you take a look at the written code behind the program. Our idea was to create a story of a couple who just broke up. Therefore, there is a lot of hate and anger in the person, sending the letter. But behind the surface, the person actually wants the partner back. 

We created the hate letter in the same frame as the original letter. 
We made a JSON-file in which we inserted the different word classes that needed to be replaced randomly. These classes consisted of two different salutations, adjectives, adverbs and nouns. Instead of calling them by their original title, we renamed them into affectionate words, so that the hate letters get a hidden meaning/message. Besides the meaning behind the JSON-file, we also named the different functions and variables used in the code 

When working with vocable code it was interesting to play around with the thought and idea of the computer as a thinking machine. Even though the computer seems intelligent enough to create this meaningful letter, filled with feelings and emotions, it is still the programmer/ the human mind behind, who has the control of what is performed. Furthermore, the machined doesn’t make any sense out of these words in the letter neither the does it understand the names we gave the different arrays – it just executes the instructions without making any sense to them. It is only human who is capable of that.
