

function setup() {
  createCanvas(800, 800);
}

function draw() {
background(249, 209, 0);

//white part
push();
fill(255);
noStroke();
rectMode(CENTER);
rect(400, 500, 110, 300, 50);
pop();

//blood part
push();
fill(240, 0, 0);
rectMode(CENTER);
noStroke();
rect(400, 600, 110, 100, 45);

//drops
rect(350, 570, 10, 60, 25);
rect(450, 570, 10, 90, 25);
rect(440, 580, 20, 70, 25);
rect(360, 580, 20, 120, 35);
rect(370, 520, 30, 90, 25);
rect(400, 545, 20, 60, 25);
rect(400, 499, 10, 10, 25);
rect(430, 640, 10, 40, 25);
rect(430, 670, 8, 8, 25);
rect(430, 680, 4, 4, 25);
pop();

//shade
push();
noStroke();
rectMode(CENTER);
fill(200);
ellipse(400, 385, 110, 70, 150);
pop();

//snor
push();
noFill();
stroke(255);
strokeWeight(5);
beginShape();
//startline
curveVertex(400, 100);
curveVertex(400, 100);
//middlline
curveVertex(350, 180);
//middlline
curveVertex(450, 280);
//endline
curveVertex(400, 370);
curveVertex(400, 370);
endShape();
pop();
}
