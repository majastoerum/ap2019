![screenshot](SS_2.png)
![screenshot](smiley.gif)
https://cdn.staticaly.com/gl/majastoerum/ap2019/raw/master/mini_ex2/p5/empty-example/index.html

For this mini exercise I have experimented with a static illusion of a tampon. 
I have investigated different shapes and colors, and how 
it is possible to assemble different objects to visualize, in this case, an emojicon.
In that connection I have especially 
explored the push() and pop()-functions and how to insert the syntaxes in the right order, to get the 
desired result. The push() and pop() functions enabled me to work with different colors and shapes without them influencing each other.
I have also tried out the curveVertex function, in this case to make the string of the 
Tampon. This was pretty difficult as I had to figure out where the string should start and end,
and where to put in the middle curveVertex’, to make the string curve as desired. 

After reading the Modifying the Universal, I really thought of how the use and perception 
of emojis has developed from being a way to only express a certain emotion or condition, to 
something people constantly relate themselves to. Now, the emojicons have to represent 
the whole world population, which is difficult. Therefore, I have made an emoji-smileyface
that randomly changes color from 0-255, when you click the mouse. Thus, nobody (hopefully) 
will get offended by the visual of this emoji - since it represents all colors.
In this case, I used the variable-syntax and function mousedPressed, to set the r, g, b 
variables randomly. 

But the emojicons has also become a cultural way to provoke certain important discussion about human rights, 
taboos, political issues and so on. I was inspired by the "Women finally get a menstruation emoji"-article to make the 
tampon-emojicon, since I thought the "blood-drop"-emojicon doesn't represent the 
idea of a period-emojicon. With an emojicon, that actually visualize
a period, the taboo about girls having such a natural thing as periods, hopefully can be broken. 
I find it quite funny, but also a little problematic, that one of the first emojicons, was an image of a poo with a face on it, 
but a pair of underpants with blood drops, is unacceptable. 


