https://glcdn.githack.com/majastoerum/ap2019/raw/master/mini_ex3/p5/empty-example/index.html 

![screenshot](SS1.gif)

For this exercise I have used the time-related function, loop-function as I desired 
to make a throbber, that continuously repeats its movements. I used the loop-function 
with a black rectangle (the same color as the background) and made it loop back to 0, 
as it went to the end of the canvas, to give the illusion of the heart monitor moving, 
as I placed the rectangle on top of the illustration. 
My idea was to create a throbber for life, since you can understand the constant heartbeat as a throbber for your life to end. 
Something is going on behind the beating sound - small processes, systems and connections, that we don’t necessarily understand – just like a computer/software.

Thinking about throbbers, it is not a thing, I normally put much attention to, – only when things are going slow – the icon will
lead to frustration and impatience. And yet, the throbber is to be seen almost everywhere, on every electronic device we are in possession of.
I think the throbber is trying to put the user in a waiting position and to assure that
something is working and trying to solve the potential problem. But the throbber doesn’t show the user, what
exactly is going on behind the, often looping movements. The movement are just forever going