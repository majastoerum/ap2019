var rect1 = 300;

function setup() {
  createCanvas(800, 800);
}

function draw() {
background(25);

//linje
push();
   stroke(180,239,0,100);
   strokeWeight(8);
   noFill();
   beginShape();
   vertex(0,400);
   vertex(200, 400);
   vertex(215, 290);
   vertex(240, 470);
   vertex(250, 400);
   vertex(270, 400);
   vertex(300, 80);
   vertex(330, 600);
   vertex(350, 400);
   vertex(390, 400);
   vertex(400, 370);
   vertex(410, 420);
   vertex(420, 400);
   vertex(800, 400);
   endShape();
pop();

//glow
push();
   stroke(180,239,0);
   strokeWeight(4);
   noFill();
   beginShape();
   vertex(0,400);
   vertex(200, 400);
   vertex(215, 290);
   vertex(240, 470);
   vertex(250, 400);
   vertex(270, 400);
   vertex(300, 80);
   vertex(330, 600);
   vertex(350, 400);
   vertex(390, 400);
   vertex(400, 370);
   vertex(410, 420);
   vertex(420, 400);
   vertex(800, 400);
   endShape();
pop();

//glow2
push();
   stroke(255);
   strokeWeight(1);
   noFill();
   beginShape();
   vertex(0,400);
   vertex(200, 400);
   vertex(215, 290);
   vertex(240, 470);
   vertex(250, 400);
   vertex(270, 400);
   vertex(300, 80);
   vertex(330, 600);
   vertex(350, 400);
   vertex(390, 400);
   vertex(400, 370);
   vertex(410, 420);
   vertex(420, 400);
   vertex(800, 400);
   endShape();
pop();


//rect
rectMode(CENTER);
noStroke();
fill(25);
rect(rect1, height/2, 500, 800);

rect1=rect1+7;
if (rect1 > 1000) {
  rect1 = -100;
}
push();
fill(255);
textSize(16);
textFont('Courier');
text('Loading death...', 470, 380);
pop();



 }
