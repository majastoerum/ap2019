https://glcdn.githack.com/majastoerum/ap2019/raw/master/mini_ex6/index.html
![screenshot](SS.gif)

In this game, I have created three different objects, that work independently of each other. The game consists of a little boy, an old spooky man/monster and one coin. The idea was to create a 2-player game, where one of the players control the boy, whose task and goal is to collect coins, and the other players task is to control the old man, whose goal is to catch the little boy. Unfortunately, I couldn’t make all these things work, so the only functions the game has right now, is the possibility of walking around in the canvas.
I played around with the play-library and wanted to create a group with the coins but couldn’t figure it out and make them disappear when the boy-object collided with them. Also I wanted something to happen, when the man catches the boy, but I didn’t have time enough to make this work.


I created a class for each object, in which I declared the functionalities I wished for the different objects. It was necessary to collect all the data that was essential to form the object I wanted. 
I needed to think of the different characters as object with different functionalities. Objects with different kinds of data in it. The idea of encapsulation of the different data, functionalities and aspects of a certain object.  
You can think of a class as a blueprint, in which you encapsulate the idea of data and functionality into an object - so, everything that means to be that specific object. 

Therefore, you can understand the object-oriented programming as a language model organized around object and data rather than the “actions” of functions. Based on the class it is possible to make instances of an object, which is defined inside of a class – this will construct the object with a specific functionality, setup, data, details, behavior and so on.

I think the object orientated programming is an interesting way to look at programming, because you also get an understanding of how coding works. You no longer understand it as a linear execution of functions and instructions, but instead you see it as this different structure, where the different codes communicate and depend on each other.

It is in the hand of the programmer to decide what’s the most necessary and important aspects and parts of an object to be represented in a program. Thereby the programmer has the power, of how the different object are represented, and thereby how a specific message or concept. Furthermore, the programmer also has the power of controlling what’s visible for the user and what’s not. The objects can contain a lot of different functionalities, data’s and so on, the user is not aware of. Therefore, there’s a tension between the code behind the object and what actually is revealed for the user. The way these objects are shown can also be perceived in different ways, and thereby the programmer also has a crucial responsibility.
