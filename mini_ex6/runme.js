let boyy;
let nissefather;
let coins;
var scl = 20;
var button;


function preload(){
  imgc=loadAnimation('coin0.png','coin1.png','coin2.png','coin3.png','coin4.png');
  imgn=loadAnimation('nissefar0.png', 'nissefar1.png');
  imgb=loadAnimation('boii0.png','boii1.png');
  imgt=loadImage('tree0.png');
  forrest=loadImage('bg0.png');
}

function setup(){
  createCanvas(800, 800);
  frameRate(20);

  boyy= new boy();
  nissefather = new nissefar();
  coins= new coin();


//instruction butto
  button=createButton("Use w,a,s,d to move the nissefather and the arrow keys to move the boy - PRESS TO HIDE THE INSTRUCTIONS");
  button.style('width','300px');
  button.position(250, 350);
  button.mousePressed(button.remove);
}

function draw(){
  //background
  image(forrest,0, 0);
  image(imgt, 580, 280);

  boyy.move();
  boyy.show();
  nissefather.move();
  nissefather.show();
  coins.show();
}

class boy {
  constructor(x, y, speed){
    this.speed=10;
    this.x=200;
    this.y=300;
  }

  move(){
    if (keyIsDown(RIGHT_ARROW)){
    this.x+=this.speed;
    }
    if (keyIsDown(LEFT_ARROW)){
    this.x-=this.speed;
    }
    if(keyIsDown(UP_ARROW)){
      this.y-=this.speed;
    }
    if (keyIsDown(DOWN_ARROW)){
    this.y+=this.speed;
    }
    this.x=constrain(this.x, 0, width-scl);
    this.y=constrain(this.y, 0, height-scl);
  }
  show(){
    animation(imgb, this.x, this.y);
  }
}

class nissefar{
  constructor(x, y, speed){
    this.x=100;
    this.y=100;
    this.speed=10;
  }

  move(){
    if (keyIsDown(68)){
      this.x+=this.speed;
    }
    if (keyIsDown(65)){
      this.x-=this.speed;
    }
    if(keyIsDown (87)){
      this.y-=this.speed;
    }
    if (keyIsDown(83)){
      this.y+=this.speed;
    }
    this.x=constrain(this.x, 0, width-scl);
    this.y=constrain(this.y, 0, height-scl);
  }

  show(){
    animation(imgn, this.x, this.y);
  }
}

class coin {
  constructor(x, y){
    this.x=500;
    this.y=700;
}
  show(){
    animation(imgc, this.x, this.y);
}
}
