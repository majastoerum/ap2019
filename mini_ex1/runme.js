
function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);

}

function draw() {
  background(25);

  translate(mouseX - width/2, mouseY - height/2);
  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.03);
  rotateZ(frameCount * 0.02);

  let c = color(210, 240, 0, 50);
  fill(c);
  stroke(210, 240, 0,);

  sphere(200);


  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.03);
  rotateZ(frameCount * 0.02);
  translate(mouseX - width/7, mouseY - height/4);

  sphere(150);

  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.03);
  rotateZ(frameCount * 0.02);
  translate(mouseX - width/5, mouseY - height/6);

  sphere(100);

  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.05);
  rotateZ(frameCount * 0.04);
  translate(mouseX - width/3, mouseY - height/4);

  sphere(50);

  rotateX(frameCount * 0.01);
  rotateY(frameCount * 0.01);
  rotateZ(frameCount * 0.01);
  translate(mouseX - width/2, mouseY - height/3);

  sphere(30);

      rotateX(frameCount * 0.01);
      rotateY(frameCount * 0.01);
      torus(50, 3);

rect(100, 100, 50, 50);
}
