dav
[link](https://cdn.staticaly.com/gl/majastoerum/ap2019/raw/master/mini_ex1/index.html?env=dev)

In this first mini exercise, I wanted to experiment with the most basic syntax'
in the p5.js program. At first, I found it a bit difficult to understand the different 
parts of the program and find a meaning in the basic "function-lines" since it was my first time programming.
But by changing the different variables and numbers, and experimenting with different 
shapes and colors, I soon got to understand how the program basically works.  
After getting more confident with the program, I discovered it was a good way to express new ideas, and revise them in different ways.
The program gives the possibilities of exploring creative ideas from another angle, which is fun. (haha)

I wanted to create something in a 3D-shape which should be able to move around in the canvas. 
So after inserting the sphere shapes in different sizes, I got the idea of creating a solarsystem. So besides the different spheres, I made them rotate around the X-, Y, and Z-axis.  
I also tried to insert torus' in the background, to create the sense of a spaciousness. This didn't work for me, as I couldn't figure out, where to insert these torus' in the function. 
They seemed to affect the speed and movements of the spheres, which got me really frustrated... so this is a thing I really want to investigate until next time. 

I also found it hard to control the motions of the spheres, and their movements seemed pretty random, since I didn't
attach any great considereation to the value of the frameCount. Therefore this is a thing I would like to investigate until my next mini exercise.
I would like to understand how the different numbers influence the movements of the different shapes, how they rotate, at how I can control them.

When I think of coding as a way of reading and writing, I both see similarities and disparities. For example when writing, it is important to spell the words right
with the correct grammar and so on, to understand the sentence right. The same applies to coding. It is important to wright the functions right and do it in the 
correct order, or else the computer is incapable of reading and understanding the command. On the other hand, when reading a text it is normally possible to interpret 
the meaning of the words and sentences in different ways. In coding, there is only one way of understanding the commands. 

![screenshot](SB1.png)