
var r;
var g;
var b;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background (255)
}

function draw() {
//hoved
ellipse(850, 500, 500);

//mund
push();
strokeWeight(8);
  beginShape();
  //start
  curveVertex(700, 580);
  curveVertex(700, 580);
//midte
curveVertex (850, 670);
//slut
curveVertex(1000, 580);
curveVertex(1000, 580);
endShape();
pop();

//øjne
push();
fill(255);
ellipse(700, 430, 60, 60);
ellipse(1000, 430, 60, 60);
pop();

//iris
push();
fill(0);
ellipse(700, 430, 30, 30);
ellipse(1000, 430, 30, 30);
pop();

}

function mousePressed (){
  r=random(0,255);
  g=random(0,255);
  b=random(0,255);
  fill(r,g,b);
}



}
