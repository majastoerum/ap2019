// var apikey = "AIzaSyBc29tplW0Q5B1CLRe_QdPd1Zbl4RB6IF8";
// var engineID = "010241859048392769631:6mwgsgmux1s";
//https://developers.google.com/custom-search/v1/
//https://cse.google.com/cse/setup/basic?cx=010241859048392769631%3A6mwgsgmux1s

let cv; //can move around
//parameters to fetch image
let url="https://www.googleapis.com/customsearch/v1?";
let apikey="AIzaSyBc29tplW0Q5B1CLRe_QdPd1Zbl4RB6IF8";
let engineID="010241859048392769631:6mwgsgmux1s";
let query="warhol+flowers";
let imgSize="small";
let request; //full URL


let getImage;

function setup(){
cv=createCanvas(500,350);
centerCanvas();
background(200,200);
frameRate(5);
fetchImage();

}

function centerCanvas(){
let x = (windowWidth-width)/2;
let y=(windowHeight-height)/2;
//console.log(x);//to finde x-value on canvas - track
cv.position(x,y);
}

function draw(){
  try{
    loadImage(getImg, function(img){
      push();
      translate(width/2-img.width/2,0);
      image(img, 0,0);
      pop();
  });
}catch (error){
  console.log(error);
  }
}
//insert google picture - google will return JSON file to get image. Need to fetch a photofunction

function fetchImage(){
  request = url+"key=" + apikey + "&cx=" + engineID + "&imgSize" + imgSize + "&q=" + query;
//console.log(request);
//when fetch you need some parameters/specifications (size, query, key)
//"&"

loadJSON(request, gotData);

}
function gotData(data){
getImg=data.item[0].pagemap.cse_thumbnail[0].src;
console.log(getImg);

}
